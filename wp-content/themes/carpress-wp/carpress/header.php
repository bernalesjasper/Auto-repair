<?php
/**
 * The Header for Carpress Theme
 *
 * @package Carpress
 * @since 1.0.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- <link rel="stylesheet" href="http://auto-repair.vamtam.com/wp-content/themes/auto-repair/vamtam/assets/js/plugins/thirdparty/shims/styles/shim.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="http://auto-repair.vamtam.com/wp-content/cache/minify/000000/LYxBEoIwDAA_RA2cfIHvcDIhSpiSQpLa6e9h1NvuYXeC1l1WTMrNM0ewOZA7fDDLjCFFH_oW5dt6VLY-jIA1SjLeUQw8euanNwla-NI_DBNQ0UCK9Cq2pTuIUq4z_97fyk8.css?ba94fe" media="all">
	<link rel="stylesheet" type="text/css" href="http://auto-repair.vamtam.com/wp-content/cache/minify/000000/M9QvSi0rzslMSS3SLyrWLcgpTc_M008uLtYvTi0pycxLLwYA.css?ba94fe" media="all">
	<link rel="stylesheet" type="text/css" href="http://auto-repair.vamtam.com/wp-content/cache/minify/000000/M9Qvz89Pzs_NTS1KTtVPLC5OLSnWTy4uRhbWzUmszC8tAQA.css?ba94fe" media="all">
	<link rel="stylesheet" id="woocommerce-smallscreen-css" href="//auto-repair.vamtam.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ba94fe" type="text/css" media="only screen and (max-width: 768px)">
	<link rel="stylesheet" type="text/css" href="http://auto-repair.vamtam.com/wp-content/cache/minify/000000/jcoxDoAwCAXQC2moJzKEfG0HSgNor--qm-vL22iaianCBcQRyCCJePNSiK-01TG4Oc1x71mhny7VAv3PVD57O5o8.css?ba94fe" media="all">
	<link rel="stylesheet" id="front-all-css" href="http://auto-repair.vamtam.com/wp-content/themes/auto-repair/cache/all.css?ba94fe" type="text/css" media="all"> -->
 	<link rel="stylesheet" type="text/css" href="http://auto-repair.vamtam.com/wp-content/cache/minify/000000/M9BPLC3J1y1KLUjMLNJNzsjMSdEvLqnMSQUA.css?ba94fe" media="all">
	<link rel="stylesheet" id="wpv-gfonts-css" href="//fonts.googleapis.com/css?family=Lato%3Anormal%2C300%2Cbold%2Citalic&amp;subset=latin&amp;ver=1" type="text/css" media="all">
	<!--  ================  -->
	<!--  = Google Fonts =  -->
	<!--  ================  -->
	<?php
	$charset = get_theme_mod( 'carpress_charset_setting', 'latin' );

	$fonts   = array( "'Open+Sans:400,700:{$charset}'", "'Dosis:400,700:{$charset}'" );
	$fonts = implode( ", ", $fonts);
	?>
	<script type="text/javascript">
		WebFontConfig = {
			google : {
				families : [<?php echo $fonts; ?>]
			}
		};
		(function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();
	</script>

	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->

		<!-- W3TC-include-js-head -->
		<?php wp_head(); ?>
		<!-- W3TC-include-css -->
	</head>
	<!-- W3TC-include-js-body-start -->

	<body <?php body_class( 'yes' === get_single_theme_mod( 'boxed' ) ? 'boxed' : '' ); ?>>
		<div class="boxed-container">
			<header class="body-header">
				<?php
				$tap_to_call_number     = get_theme_mod( 'tap_to_call_number' );
				$tap_to_call_text       = get_theme_mod( 'tap_to_call_text' );
				$tap_to_call_visibility = get_theme_mod( 'tap_to_call_visibility', 'mobile' );

				if ( ! empty( $tap_to_call_number ) && ! empty( $tap_to_call_text ) ) :
				?>
				<a href="tel:<?php echo $tap_to_call_number; ?>" class="tap-to-call<?php echo 'mobile' === $tap_to_call_visibility ? '  hidden-desktop' : ''; ?>"><?php echo $tap_to_call_text; ?></a>
				<?php
					endif;
				?>
				<div class="navbar  navbar-inverse  navbar-fixed-top <?php echo 'sticky' === get_theme_mod( 'navbar_position', 'sticky' ) ? ' js--navbar' : ''; echo 'mobile' !== $tap_to_call_visibility ? '  more-down' : ''; ?>">
					<div class="container">
						<div class="header-padding  clearfix">
							<!--  =========================================  -->
							<!--  = Used for showing navigation on mobile =  -->
							<!--  =========================================  -->
							<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>

							<!--  ==============================  -->
							<!--  = Place for logo and tagline =  -->
							<!--  ==============================  -->
							<a class="brand<?php echo 'yes' === get_theme_mod( 'logo_img_retina', 'yes' ) ? '  retina' : ''; ?>" href="<?php echo home_url(); ?>">
							<?php
								$logo = get_theme_mod( 'logo_img', '' );
								if ( empty( $logo ) ) :
							?>
								<h1>
									<?php echo colored_title( get_bloginfo( 'title' ) ); ?>
								</h1>
								<span class="tagline"><?php echo get_bloginfo( 'description' ); ?></span>
							<?php
								else:
							?>
								<img src="<?php echo $logo; ?>" alt="<?php echo esc_attr( get_bloginfo( 'title' ) ); ?>" />
							<?php
								endif;
							?>
							</a>

							<!--  =============================================  -->
							<!--  = Main top navigation with drop-drown menus =  -->
							<!--  =============================================  -->
							<div class="nav-collapse collapse">
							<?php
								if ( has_nav_menu( 'main-menu' ) ) {
									$args = array(
										'theme_location' => 'main-menu',
										'container'      => false,
										'menu_class'     => 'nav',
										'echo'           => true,
										'depth'          => 3,
									);
									wp_nav_menu( $args );
								}
								$btn_page_id = ot_get_option( 'appointment_button', null );
								if ( null != $btn_page_id ) :
									if ( function_exists( 'icl_object_id' ) ) {
										$btn_page_id = icl_object_id( $btn_page_id, 'page', true );
									}
									$btn_page = get_page( $btn_page_id );
							?>
								<a href="<?php echo get_page_link( $btn_page->ID ); ?>" class="btn btn-featured pull-right"><?php echo $btn_page->post_title; ?></a>
							<?php
								endif;
							?>
							</div><!-- /.nav-collapse-->
						</div>
					</div>
				</div>
			</header>