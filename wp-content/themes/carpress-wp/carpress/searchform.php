<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" class="form">
	<div class="input-append">
		<input name="s" class="search-width" id="appendedInputButton" type="text" placeholder="Search"/>
		<button class="btn btn-theme" type="submit"><?php _e( 'Go' , 'carpress_wp'); ?></button>
	</div>
</form>
